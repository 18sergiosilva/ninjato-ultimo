<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package storefront
 */

?>
<?php
$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="<?php storefront_homepage_content_styles(); ?>"
	data-featured-image="<?php echo esc_url( $featured_image ); ?>">
	<div class="col-full">
		<?php
		/**
		 * Functions hooked in to storefront_page add_action
		 *
		 * @hooked storefront_homepage_header      - 10
		 * @hooked storefront_page_content         - 20
		 */
		// do_action( 'storefront_homepage' );
		?>
	</div>
</div><!-- #post-## -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>



<!--<section>
 <div class="title__sectionq">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.gif" alt="">
</div>


<video autoplay muted loop width="1920" height="720" id="myVideo">
  <source src="<?php echo get_template_directory_uri(); ?>/assets/images/inicial.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

</section>-->



<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

<div class="title__section90">
		<div class="centrado90"><a href="#about">ACERCA</a></div>
</div>
<div class="title__section91">
		<div class="centrado91"><a href="#proyect">PROYECTOS</a></div>
</div>
<div class="title__section92">
		<div class="centrado92"><a href="#client">CLIENTES</a></div>
</div>
<div class="title__section93">
		<div class="centrado93"><a href="#infos">CONTACTO</a></div>
</div>
  <div class="carousel-inner">
	<div class="carousel-item active">
		<video autoplay muted loop id="myVideo">
  			<source src="<?php echo get_template_directory_uri(); ?>/assets/images/inicial.mp4" type="video/mp4">
  			Your browser does not support HTML5 video.
		</video>
	</div>
 </div>
  <div class="nav-social">
	<a href="https://www.facebook.com/ninjatobtl/">
	<div class="mycard">
    	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/SM-011.png" alt="Card Back">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/SM-02.png" class="img-top" alt="Card Front">
    </div>
    </a>
	<a href="https://instagram.com/ninjatobtl?igshid=1ohzsgs7m9bcd">
	<div class="mycard">
    	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/SM-044.png" alt="Card Back">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/SM-03.png" class="img-top" alt="Card Front">
    </div>
    </a>
</div>

<div class="nav-social2">
    <a href="#">
    	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/jis.gif" alt="">
    </a>
</div>
</div>

<section id="about">

<div class="w3-animate-opacity title__section3">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/ABOUT.png" alt="Nueos Ingresos">
</div>

<div class="w3-animate-opacity title__section4">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/US2.gif" alt="Nueos Ingresos">
</div>

<div class="w3-animate-opacity title__section5">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/texto.png" alt="Nueos Ingresos">
</div>


</section>


<section id="smart">

<div class="w3-animate-opacity title__section6">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/SMART45.gif" alt="Nueos Ingresos">
</div>


</section>


<div class="w3-animate-opacity title__section200">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/proyect.png" alt="Nueos Ingresos">
</div>
<div class="containermio" id="proyect">
  <div class="row">
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="19"]' ); ?>
    </div>
	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="38"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="40"]' ); ?>
    </div>
	<div class="col" data-aos="fade-up">
	    <?php echo do_shortcode( '[popup_anything id="42"]' ); ?>
    </div>
  </div>
  <div class="row">
  	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="44"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="46"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="48"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="50"]' ); ?>
    </div>
  </div>
  <div class="row">
  	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="54"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="56"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="58"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="60"]' ); ?>
    </div>
  </div>
  <div class="row">
  	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="9912"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="102"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="99"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="3291"]' ); ?>
    </div>
  </div>
</div>

<div class="containermio2" id="proyect2">
  <div class="row">
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="19"]' ); ?>
    </div>
	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="38"]' ); ?>
    </div>
  </div>
  <div class="row">
  	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="40"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="42"]' ); ?>
    </div>
  </div>
  <div class="row">
  	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="44"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="46"]' ); ?>
    </div>
  </div>
  <div class="row">
  	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="48"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="50"]' ); ?>
    </div>
  </div>
  <div class="row">
  	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="54"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="56"]' ); ?>
    </div>
  </div>
  <div class="row">
  	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="58"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="60"]' ); ?>
    </div>
  </div>
  <div class="row">
  	<div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="102"]' ); ?>
    </div>
    <div class="col" data-aos="fade-up">
		<?php echo do_shortcode( '[popup_anything id="99"]' ); ?>
    </div>
  </div>
</div>



</section>
<video autoplay muted loop id="client">
  	<source src="<?php echo get_template_directory_uri(); ?>/assets/images/CLIENTES1.mp4" type="video/mp4">
  	Your browser does not support HTML5 video.
</video>

<section id="infos">
<section id="infos2">

<div class="title__section100">
	<div class="centrado100">CDMX - (55)4165-5833 // Chihuahua - (614)410-0537</div>
</div>

</section>
</section>

	  <!--



		  <section id="cliente">
<div class="w3-animate-opacity title__section8">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/clientefont.png" alt="Nueos Ingresos">
</div>
<div class="w3-animate-opacity title__section7">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/GIF_Clientes.gif" alt="Nueos Ingresos">
</div>

<section id="jump">
<div class=" title__section77">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/jujus.png" alt="Nueos Ingresos">
</div>
</section>

		  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
	<div class="row">
	<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
		</div>
    </div>
    <div class="carousel-item">
	<div class="row">
	<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
		</div>
    </div>
    <div class="carousel-item">
	<div class="row">
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
		</div>
    </div>
  </div>
</div>
	  
	  <section>
<video autoplay muted loop id="myVideo3">
  <source src="<?php echo get_template_directory_uri(); ?>/assets/images/contacto.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
</section>


<section id="about">

</section>


<video autoplay muted loop id="myVideo2">
  <source src="<?php echo get_template_directory_uri(); ?>/assets/images/SMART.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

<section id="smart">

</section>

<section id="proyectos">

</section>

<section id="cliente">

</section>


<video autoplay muted loop id="myVideo3">
  <source src="<?php echo get_template_directory_uri(); ?>/assets/images/contacto.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

<section id="contacto2">

</section>-->

	
	
	

